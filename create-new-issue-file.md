<!--
@issue create-new-issue-file
@title Create New Issue File command
@description
 It would be nice if there was a command line command for
 automatically generating a new issue file with some default
 tags, such as title, description, priority and due date or
 milestone.

 Similar pattern generators would be useful for popular IDEs,
 such as PyCharm and Intellij.
->
