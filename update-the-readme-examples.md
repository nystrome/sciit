#@issue update-the-readme-examples
#@title Readme Examples Need Updating
#@description:
#The readme examples don't conform with the actual format, e.g. of having
#an ID slug after @issue and separate @title.

#@priority high
